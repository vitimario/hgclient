# README #

The Server version of this app is available at  
https://vitimario@bitbucket.org/vitimario/hgserver.git

Documentation @  
https://drive.google.com/file/d/0B6wlleL2j4psLXZ4N3Z0dEZDRDQ/view

### What is this repository for? ###

* Networking exam UNIPI RCL
* Version 0.9

### Execution example ###

Wellcome  
Hang Man client running...  
type "login" to login or "register" to register a new user  
*register*  
name:  
*a*  
password:  
*a*  
type "login" to login or "register" to register a new user  
*login*  
name:  
*a*
password:  
*a*  
{}  
logged in  
{"b":{"numJoined":0,"numLetters":"4","guesser":{},"numGuessers":"1"}}  
*join_game b*  
Client.TaskManager: join_game  
{}  
Client.TaskManager: guesserGameReady  
Client.TaskManager: guesser game starting... press ENTER to play  
  
Client.GuesserGame: \*\*\*\*  
Client.GuesserGame: make a guess  
*a*  
Client.GuesserGame: sending...  
Client.GuesserHelper: \*\*a\* 0  
*c*  
Client.GuesserGame: sending...  
Client.GuesserHelper: c\*a\* 0  
*i*  
Client.GuesserGame: sending...  
Client.GuesserHelper: cia\* 0  
*o*  
Client.GuesserGame: sending...  
Client.GuesserHelper: YOU WIN 0  
Client.GuesserGame: game has ended...  
CTRL+C to quit  

### How do I get set up? ###

Eclipse-> import -> Git -> Projects From Git -> clone URI -> specify the URI to clone
this runs under JRE IcedTea 2.5.4